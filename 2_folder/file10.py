'''
Write a program to take two numbers as input parameter and then ask for the arithmetic parameter to be performed.
i.e “Enter Two numbers”
10 45
“Operations to perform “   +

Sum is 55
'''
value1 = int(input("Enter value1: "))
value2 = int(input("Enter value2: "))
print(value1,value2)
opr = input("Operations to perform: ")
switcher = {
    '+': value1+value2,
    '*': value1*value2,
    '/': value1/value2,
    '-': value1-value2,
    '^': value1**value2,
    '%': value1%value2
}
print(switcher.get(opr,"Invalid"))