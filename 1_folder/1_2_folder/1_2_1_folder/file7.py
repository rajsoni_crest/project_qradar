'''
Write python script to take no of arguments as input from the user. Then read no of arguments from the standard input.
Print read arguments on output.
'''
import sys
ARG = []
for _ in range(int(sys.argv[1])):
    ARG.append(input())
print(ARG)
