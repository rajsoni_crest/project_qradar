'''
Write python script to take one integer argument and then print as follows:
        - If Value >0 and Value < 10 — Small
        - If Value > 10 and Value <100 — Medium
        - If Value <1000 — Large
        - If Value > 1000 — Invalid 
'''
val = 0
try :
    val = int(input("Enter Value: "))    
except ValueError:
    print("Enter interger value only")
    import sys
    sys.exit()

if val > 0 and val <= 10:
    print("Small")
elif val > 10 and val <= 100:
    print("Medium")
elif val <= 1000 and val > 100:
    print("Large")
elif val > 1000:
    print("Invalid")